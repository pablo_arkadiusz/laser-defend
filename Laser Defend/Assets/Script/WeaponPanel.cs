﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPanel : MonoBehaviour //weapon panel used in hangar_scene to select the player weapon
{
    [SerializeField] private Text weaponInfoText;
    [SerializeField] private Button btnMachineGun, btnLaser, btnRipple, btnBall, btnRocket;
    [SerializeField] private AnimationManager animationManager; //the animation manager manage the movement of the sprite of the selected weapon
    private Image actualTarget;
    private Color targetColor = new Color(10, 255, 0);

    private void Start()
    {
        SelectButtonFromData();
    }

    public void TargetThisButton(Button button)
    {
        if (actualTarget != null)
            actualTarget.color = Color.white;
        actualTarget = button.GetComponent(typeof(Image)) as Image;
        actualTarget.color = targetColor;
        animationManager.ChangeTarget(actualTarget.transform.GetChild(0)); //change the target of the animation "up and down" (set it to the ship image (which is the children of the target button))
        UpdateText();
    }


    private void UpdateText()
    {
        switch(actualTarget.name)
        {
            case "MachineGun":
                weaponInfoText.text = "MachineGun: Very fast, over upgrade increases number of bullets fired and fire-rate, hits only one enemy for every gun";
                GameData.gameData.weaponSelected = Weapons.MachineGun;
                break;
            case "Laser":
                weaponInfoText.text = "Laser Sword. Be like a yedai, over upgrade increases number of laser swords and laser-range, tits only one enemy at time per laser";
                GameData.gameData.weaponSelected = Weapons.Laser;
                break;

            case "Ripple":
                weaponInfoText.text = "Ripple: Probably the best best Area of Efect, over upgrade increases area of effect and fire-rate. It dont destroy on hit so can hit multiple enemy at time";
                GameData.gameData.weaponSelected = Weapons.Ripple;
                break;

            case "Ball":
                weaponInfoText.text = "Ball: Strong and precise, but very slow shooting-rate and low area of efect";
                GameData.gameData.weaponSelected = Weapons.Ball;
                break;

            case "Rocket":
                weaponInfoText.text = "Rocket, if you like strenght despite of all this is your weapon, but it may be dificult to aim... ";
                GameData.gameData.weaponSelected = Weapons.Rocket;
                break;
        }
        GameData.SaveData();
    }

    private void SelectButtonFromData()
    {
        switch(GameData.gameData.weaponSelected)
        {
            case Weapons.MachineGun:
                TargetThisButton(btnMachineGun);
                return;
            case Weapons.Laser:
                TargetThisButton(btnLaser);
                return;
            case Weapons.Ball:
                TargetThisButton(btnBall);
                return;
            case Weapons.Ripple:
                TargetThisButton(btnRipple);
                return;
            case Weapons.Rocket:
                TargetThisButton(btnRocket);
                return;
        }
    }

}
