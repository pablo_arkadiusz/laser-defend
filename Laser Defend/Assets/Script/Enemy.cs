﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathFollow))]
[RequireComponent(typeof(PolygonCollider2D))]

public class Enemy : NonFriendly
{
    [Header("Enemy")]
    [SerializeField] private float health = 100;
    [SerializeField] private int damage;

    //SCORE AND ITEMS UPDATE
    [SerializeField] private int pointsForKill = 1;
    private ItemSpawnManager itemSpawnManager;
    private ScoreManager scoreManager;

    [Header("Proyectile")]
    [SerializeField] private GameObject proyectilePrefab;
    [SerializeField] private float minTimeBeetwenShots = 0.2f;
    [SerializeField] private float maxTimeBeetwenShots = 3f;
    [SerializeField] private float bulletSpeed = 10f;

    [Header("Sound")]
    [SerializeField] private AudioClip deathSFX;
    [SerializeField] [Range(0,1)]private float deathSoundvolume = 0.3f;
    [SerializeField] private AudioClip shootSFX;
    [SerializeField] [Range(0, 1)] private float shootSoundVolume = 0.7f;

    public float Speed { get; private set; } //Setted in the WaveConfig that the EnemySpawner gets to create enemies
    private bool isOnCooldown = true; //shooting cooldown get random cooldown between minTimeBetweenShots and maxTimeBetweenShots
    private bool dying;

    private void Start()
    {
        //This variables cant be serialized because enemies are prefabs
        itemSpawnManager = FindObjectOfType(typeof(ItemSpawnManager)) as ItemSpawnManager;
        scoreManager = FindObjectOfType(typeof(ScoreManager)) as ScoreManager;

        StartCoroutine(ChargeCooldown());
        (GetComponent(typeof(DamageDealer)) as DamageDealer).Damage = damage * 10; //damage of contact
    }

    private void Update()
    {
        if (!isOnCooldown && !dying)
            Shoot();
    }

    private void Shoot()
    {
        Vector2 pos = (Vector2)transform.position - new Vector2(0, 0.5f);
        Rigidbody2D bullet = Instantiate(proyectilePrefab, pos, Quaternion.identity).GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        bullet.velocity = new Vector2(0, -bulletSpeed);
        (bullet.GetComponent(typeof(DamageDealer)) as DamageDealer).Damage = damage; //Sets the damage of the DamageDealer of the bullet
        isOnCooldown = true;
        AudioSource.PlayClipAtPoint(shootSFX, Camera.main.transform.position, shootSoundVolume);
        StartCoroutine(ChargeCooldown());
    }

    private void SetBulletDamage(DamageDealer bullet)
    {
        bullet.Damage = damage;
    }

    private IEnumerator ChargeCooldown() //shooting cooldown get random cooldown between minTimeBetweenShots and maxTimeBetweenShots
    {
        float cd = UnityEngine.Random.Range(minTimeBeetwenShots, maxTimeBeetwenShots);
        yield return new WaitForSeconds(cd);
        isOnCooldown = false;
    }

    protected override void SetPathToFollow(Path path) 
    {
        PathFollow pathFollow = GetComponent(typeof(PathFollow)) as PathFollow;
        pathFollow.PathToFollow = path;
        pathFollow.speed = Speed;


    }
    public override void SetConfig(WaveConfig waveConfig) //Set Configs from WaveConfig
    {
        Speed = waveConfig.GetMoveSpeed();
        SetPathToFollow(waveConfig.GetPathToFollow());
        (GetComponent(typeof(PathFollow)) as PathFollow).destroyAtEndOfPAth = waveConfig.GetDestroyAtEndOfPath();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Actually it only collide with player and player bullets
        DamageDealer damageDealer = other.gameObject.GetComponent(typeof(DamageDealer)) as DamageDealer;
        if (damageDealer)
        {
            damageDealer.Hit();
            if (!dying)
                ProcessHit(damageDealer.Damage);
        }
    }

    private bool onlyOnce=true; //To dont activate the death setting again. Enemy only can die one time
    public override void ProcessHit(float dmg) //is called by laser too
    {
        health -= dmg;
        if (health <= 0 && onlyOnce) //if enemy died, only can be called one time. Enemy gameobject still exist but it change sprite to the death sprites through the animator
        {                                                                           //So it should be activated only one time. After the animation the gameobject is destroyed
            onlyOnce = false;
            AudioSource.PlayClipAtPoint(deathSFX, Camera.main.transform.position, deathSoundvolume);
            itemSpawnManager.SpawnItems(transform.position);
            dying = true;
            GetComponent<Animator>().SetTrigger("die");
        }
    }

    public int GetPoints()
    {
        return pointsForKill;
    }
    

    public override void Die() 
    {//called from animator after the animation of death
        scoreManager.AddScore(pointsForKill);
        (FindObjectOfType(typeof(WaveManager)) as WaveManager).EnemyDeath();
        Destroy(gameObject);
    }
}
