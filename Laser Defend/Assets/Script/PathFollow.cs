﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PathFollow : MonoBehaviour
    //path follow system that uses NonFriendly objects to follow a path (Enemies and asteroids) and also the ship selector in the Planet_menu_scene
{
    [HideInInspector] public bool destroyAtEndOfPAth;
    [SerializeField] private bool changeRotation;
    [SerializeField] private bool startAutomatically = true;

    //Get this from WavePath
    public Path PathToFollow{ private get; set; }
    private int nextPosIndex = 0; //index position to follow
    
    [HideInInspector]public float speed;

    private Coroutine followingPathCoroutine;

    private bool forward=true;

    [SerializeField]private float reachDistance = 0.5f;

    private void Start()
    {
        if (startAutomatically)
        {
            if (PathToFollow.IsDoubleDirection())
                StartFollowingLoopingPath();
            else
                StartFollowingOnedirectionalPath();
        }
    }

    private IEnumerator FollowLooping() //follow the path in two directions (when reach the end it dont back to the first position, instead it change the direction)
    {
        while (true)
        {
            Vector3 nextPos = PathToFollow.GetWayPointPos(nextPosIndex);
            float distance = Vector2.Distance(nextPos, transform.position);
            transform.position = Vector2.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);

            if (changeRotation)
            {
                Vector3 targetDir = nextPos - transform.position;
                float step = 5f * Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0);
                transform.rotation = Quaternion.LookRotation(newDir);
            }

            if (distance < reachDistance)
            {
                if (forward)
                {
                    if (!MoveForward())
                    {
                        if (destroyAtEndOfPAth)
                            (GetComponent(typeof(NonFriendly)) as NonFriendly).Die();
                        forward = false;
                    }
                }
                else if (!MoveBackwards())
                    forward = true;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FollowOneDirection() //when reach the end of path it back to the first point
    {
        while (true)
        {
            Vector3 nextPos = PathToFollow.GetWayPointPos(nextPosIndex);
            float distance = Vector2.Distance(nextPos, transform.position);
            transform.position = Vector2.MoveTowards(transform.position, nextPos, speed * Time.deltaTime);

            if (changeRotation)
            {
                Vector3 targetDir = nextPos - transform.position;
                float step = 5 * Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0);
                transform.rotation = Quaternion.LookRotation(newDir);
            }

            if (distance < reachDistance)
            {
                if (nextPosIndex != PathToFollow.GetLength() - 1)
                    ++nextPosIndex;
                else if (destroyAtEndOfPAth)
                    (GetComponent(typeof(NonFriendly)) as NonFriendly).Die();
                else nextPosIndex = 0;
            }  
            yield return new WaitForEndOfFrame();
        }
    }

    private bool MoveForward()
    {
        ++nextPosIndex;
        return nextPosIndex != PathToFollow.GetLength() -1 ; //if is not at end returs true to continue moving forwards
    }

    private bool MoveBackwards()
    {
        --nextPosIndex;
        return nextPosIndex != 0; //if is not at begin returns true to continue moving backwards
    }

    public void StartFollowingOnedirectionalPath()
    {
        if(followingPathCoroutine==null)
            followingPathCoroutine = StartCoroutine(FollowOneDirection());
    }

    public void StartFollowingLoopingPath()
    {
        if (followingPathCoroutine == null)
            followingPathCoroutine = StartCoroutine(FollowLooping());
    }

    public void StopFollowingPath()
    {
        if (followingPathCoroutine != null)
        {
            StopCoroutine(followingPathCoroutine);
            followingPathCoroutine = null;
        }
    }

    public int GetLastIndex()
    {
        return nextPosIndex;
    }
    public void SetIndex(int newIndex)
    {
        nextPosIndex = newIndex;
    }
    public void SetForward(bool value)
    {
        forward = value;
    }
}
