﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : WeaponClass
{
    [Header("Proyectile")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float bulletSpeed=10f;
    private const float BASE_DAMAGE=1f; 

    [Header("Sound")]
    [SerializeField] private AudioClip shootSFX;
    [SerializeField] [Range(0, 1)] private float shootSoundVolume = 0.7f;


    protected override float GetBaseDamage()
    { 
        return BASE_DAMAGE;
    }

    override public void Fire() //CALLED FROM PLAYER. WHEN PLAYER FIRE AND HIS WEAPON IS A MACHNE GUN, THIS METHOD SELECT THE ACTUAL MODIFIER AND SHOOT IN BASE OF THAT. 
                                //IT CHANGE THE NUMBER OF BULLETS FOR EVERY MODIFIER
    {
        if (!isOnCooldown)
        {
            if (WeaponModifier == 1)
                CentralBullet();
            
            else if (WeaponModifier == 2)
                FireFirstRowBullet();
            
            else if (WeaponModifier == 3)
            {
                CentralBullet();
                FireFirstRowBullet(0.5f);
            }
            else if (WeaponModifier == 4)
            {
                CentralBullet();
                FireFirstRowBullet(0.5f);
                FireSecondRowBullet(1f);
            }
            else if (WeaponModifier == 5)
            {
                CentralBullet();
                FireFirstRowBullet(0.5f);
                FireSecondRowBullet(1f);
                FireThirdRowBullet(2f);
            }
            isOnCooldown = true;
            AudioSource.PlayClipAtPoint(shootSFX, Camera.main.transform.position, shootSoundVolume);
            StartCoroutine(chargeCooldown());
        }
    }

    private void CentralBullet() // CREATE BULLET IN CENTRAL POSITION
    {
        CreateCentralBullet(bulletPrefab, bulletSpeed); // INHERITED FROM PARENT
    }
    private void FireFirstRowBullet(float rotation = 0.25f) // CREATE TWO SIMETRICAL BULLETS IN THE FIRST ROW
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.2f,0, rotation, bulletSpeed);
    }
    private void FireSecondRowBullet(float rotation = 0.6f)//INHERITED FROM PARENT
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.4f,0, rotation, bulletSpeed); // CREATE TWO SIMETRICAL BULLETS IN THE SECOND ROW
    }
    private void FireThirdRowBullet(float rotation = 0.8f)//INHERITED FROM PARENT
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.6f,0, rotation, bulletSpeed); // CREATE TWO SIMETRICAL BULLETS IN THE THIRD ROW
    }


    private IEnumerator chargeCooldown()
    {
        yield return new WaitForSeconds(secondAttribute);
        isOnCooldown = false;
    }
}
