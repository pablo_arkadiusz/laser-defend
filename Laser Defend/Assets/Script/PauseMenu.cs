﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour //pause the game and appear a pause_panel
{

    public static bool gameIsPaused;
    [SerializeField] private GameObject pauseMenu;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!gameIsPaused)
                Pause();
            else
                Resume();
        }
    }

    public void Pause()
    {
        MakeCursorVisible();
        (pauseMenu.GetComponentInChildren(typeof(Animator)) as Animator).SetTrigger("in");  
        gameIsPaused = true;
        Time.timeScale = 0f;
        Color translucid = new Color(255, 255, 255, 0.75f);
        (pauseMenu.GetComponent(typeof(Image)) as Image).color = translucid;

    }
    public void Resume()
    {
        Cursor.visible = false ;
        (pauseMenu.GetComponentInChildren(typeof(Animator)) as Animator).SetTrigger("out");
        Time.timeScale = 1f;
        gameIsPaused = false;

        Color invisible = new Color(255, 255, 255, 0);
        (pauseMenu.GetComponent(typeof(Image)) as Image).color = invisible;
    }
    public void MakeCursorVisible()
    {
        Cursor.visible = true;
    }

}
