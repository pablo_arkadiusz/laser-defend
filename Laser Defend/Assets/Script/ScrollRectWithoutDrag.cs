﻿using UnityEngine.UI;
using UnityEngine.EventSystems;



public class ScrollRectWithoutDrag : ScrollRect //the shop uses scroll rect but dont need the drag behaviour, because it works with buttons to change pages. So this override that behaviour whith empty body 
{
    public override void OnBeginDrag(PointerEventData eventData) { }
    public override void OnDrag(PointerEventData eventData) { }
    public override void OnEndDrag(PointerEventData eventData) { }
}
