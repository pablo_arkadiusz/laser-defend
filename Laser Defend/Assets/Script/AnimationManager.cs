﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    //Script that manage the rotation and move animations of the objects in the Hangar Scene

    [SerializeField] private Transform[] objectsToRotate; //ships that rotate in the shop panel
    [SerializeField] private Transform slowRotateObj; //the ship selected in the hangar rotate slowly
    private Transform objectToMoveUpAndDown; //The start weapon selected move top and down


    private Coroutine movingCoroutine; //coroutine to move top and down the weapon selected
    private Vector2 startPos;

    //move top and bot variables
    byte contador;
    bool moveTop;


    private void Start()
    {
        StartMovingTopAndDown();
    }
    void Update ()
    {
        foreach (Transform obj in objectsToRotate)
            obj.Rotate(0, 0, 3);
        slowRotateObj.Rotate(0, 0, 1);
      
    }


    public void ChangeTarget(Transform newTarget)
    {
        if (objectToMoveUpAndDown) //if there are already a weapon animated, it go back to original position and stop the animation
        {
            objectToMoveUpAndDown.transform.position = startPos; 
            //StopMoving(); 
        }
        objectToMoveUpAndDown = newTarget;
        startPos = objectToMoveUpAndDown.transform.position;
        //StartMoving();
    }

    public void ChangeSlowlyRotatedObj(Transform newObj) //called from button when player change ship
    {
        slowRotateObj = newObj;
    }

    private void StartMovingTopAndDown()
    {
        StartCoroutine(MovingTopAndDown());
    }

    private IEnumerator MovingTopAndDown()
    {
        //a little offset top to stay in the frame correctly
        byte contador = 0;
        bool moveTop = false;
        do
        {
            objectToMoveUpAndDown.Translate(Vector2.up * Time.deltaTime * 25);
            ++contador;
            yield return new WaitForEndOfFrame();
        }while (contador < 10);

        while (true)
        {
            //moving top and bot
            if (contador > 40) { moveTop = !moveTop; contador = 0; }
            if (moveTop)
                objectToMoveUpAndDown.Translate(Vector2.up * Time.deltaTime * 25);
            else
                objectToMoveUpAndDown.Translate(Vector2.down * Time.deltaTime * 25);
            ++contador;
            yield return new WaitForEndOfFrame();
        }
    }

}
