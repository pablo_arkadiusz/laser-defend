﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Enemy))]

public class Boss : MonoBehaviour
    //The boss spawn a special abillity every X time (random time between min and max)
    //The special abillity spawns 18 bullets in all directions
{
    [SerializeField] private float minTimeToAbillity=5f;
    [SerializeField] private float maxTimeToAbillity=15f;
    [SerializeField] private GameObject specialGunPrefab;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private int damage;

    private void Start()
    {
        StartCoroutine(CalculateTimeForHability());
    }

    private IEnumerator CalculateTimeForHability()
    {
        yield return new WaitForSeconds(Random.Range(minTimeToAbillity, maxTimeToAbillity));
        UseSpecialAbillity();
    }

    private void UseSpecialAbillity()
    {
        CreateDirectionalShoot();
        StartCoroutine(CalculateTimeForHability());
    }

    private void CreateDirectionalShoot()  //Shoot 18 bullets in every directions (one per every 20 degrees)
    {
        int degrees = 0;
        for(byte i=0;i<18;++i, degrees += 20)
        {
            Rigidbody2D bullet = Instantiate(specialGunPrefab, transform.position, Quaternion.identity).GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
            (bullet.GetComponent(typeof(DamageDealer)) as DamageDealer).Damage = damage;
            bullet.velocity = RotateVector(degrees)* bulletSpeed;
        }
    }


    private Vector2 RotateVector(float a) // Create two points going top (0,1) and rotate it the degrees needed (a). Used to set the velocity of bullets in every angle possible
    {                                                      
        a *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(a), Mathf.Sin(a));
    }

}
