﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour //scroll used in the planet_menu_scene
{
    [SerializeField] Transform frame;
    private float maxDist;

    private void Start()
    {
        maxDist=frame.position.y - transform.position.y;
    }

    void Update ()
    {
        float dist = frame.position.y - transform.position.y;
        Vector3 move = new Vector2(0, Input.GetAxis("Mouse ScrollWheel") * 5);

        if (Input.GetAxis("Mouse ScrollWheel") > 0 && dist > maxDist) // forward
            transform.position += move;
        
        else if (Input.GetAxis("Mouse ScrollWheel") < 0 && dist < -maxDist) // backwards

            transform.position += move;
        

        
    }
}
