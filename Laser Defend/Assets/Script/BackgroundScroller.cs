﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
    //Script that create the moving effect of the background
{

    [SerializeField] private float scrollSpeed;

    private Material myMaterial;
    private Vector2 offset;

    void Start()
    {
        myMaterial = (GetComponent(typeof(Renderer)) as Renderer).material; //the amterial of mesh renderer
        offset = new Vector2(0f,scrollSpeed);
    }

    private void Update()
    {
        myMaterial.mainTextureOffset += offset * Time.deltaTime; //When the material of a Mesh Renderer change the offset it create a moving effect
    }
}
