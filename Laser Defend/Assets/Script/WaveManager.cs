﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//MANAGES THE WAVE_PANEL CALLING PANEL_CONTROLLER AND WHEN ENEMIES START AND STOP SPAWNING CALLING ENEMY_SPAWNER
public class WaveManager : MonoBehaviour
{
    [SerializeField]private GameObject wavePanel;
    [SerializeField] private EnemySpawner enemySpawner;
    private PanelController panelController;

    [HideInInspector] public bool waveEnded;
    private Text waveText;

    private static int level;
    private byte actualWave;
    private byte lastWave = 11;
   
    private uint enemiesAlive=0;


    private void Awake()
    {
        level = GameData.gameData.levelSelected;    
        if (level == 0) level = 1;
        actualWave = 1;
        
    }

    private void Start()
    {
        panelController = FindObjectOfType(typeof(PanelController)) as PanelController;
        waveText = wavePanel.transform.GetChild(1).GetComponent(typeof(Text)) as Text; //the second child of the panel is the modificable text
        DisplayPanel();
    }


    public void SetLevel(int level)
    {
        WaveManager.level = level;
    }

    public  void EnemyBorn()
    {
        ++enemiesAlive;
    }

    public void EnemyDeath()
    {
        --enemiesAlive;
        CheckIfNoEnemiesLeft();
    }

    public static int GetActualWorld()
    {
        return level;
    }


    public void CheckIfNoEnemiesLeft()
    {

        if (enemiesAlive == 0 && waveEnded)
        {
            ++actualWave;
            if (actualWave > lastWave)
                panelController.ShowLastLevelPanel();
            else
            {
                DisplayPanel();
                waveEnded = false;
            }
        }
    }

    private void DisplayPanel()
    {
        UpdateWaveText();
        panelController.AppearWavePanel();
        StartCoroutine(StartSpawningWavesInSec(5));
    }

    private IEnumerator StartSpawningWavesInSec(float sec)
    {
        yield return new WaitForSeconds(sec);
        panelController.DisappearWavePanel();
        enemySpawner.SpawnAllWaves();
    }

    private void UpdateWaveText()
    {
        panelController.UpdateWaveTexts(actualWave); 
    }

    public WaveConfig[] GetWaves()
    {
        return Resources.LoadAll<WaveConfig>(("Waves/Level " + level + "/Wave " + actualWave));
    }
}
