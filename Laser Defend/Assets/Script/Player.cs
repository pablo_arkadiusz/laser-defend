﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour
{
    
    [Header("Player")]
    [SerializeField] private int health = 5;
    public float ShipDamageBonus { set; get; }
    private Vector3 mousePosition;
    private float moveSpeed = 0.1f;
    private byte weapon_upgrade_lvl;
    private const byte MAX_LEVEL = 20;
    private byte Level { set { weapon_upgrade_lvl = value > MAX_LEVEL ? MAX_LEVEL : value; } get { return weapon_upgrade_lvl; } } //fit level between 0 and 12

    [Header("Sound")]
    [SerializeField] private AudioClip deathSFX;
    [SerializeField] private AudioClip loseLife;
    [SerializeField] [Range(0, 1)] private float deathSoundVolume = 0.7f;

    //variables to fit the player ship in the camera view screen
    private float paddingX = 0.1f;
    private float paddingY = 0.1f;
    private float xMin, xMax, yMin, yMax; //to create margin limit in movement

    private bool dying;
    
    private WeaponClass weaponSelected;

    private PanelController panelController;

    void Awake ()
    {
        SetShipStats(); 
        SelectWeapon(GameData.gameData.weaponSelected);
        weapon_upgrade_lvl = 1;
        
        weaponSelected.EquipWeapon(GetWeapon(), weapon_upgrade_lvl);
        
        SetUpMoveBoundaries();

        panelController = FindObjectOfType(typeof(PanelController)) as PanelController;
        panelController.UpdateLife(health);

        weaponSelected.ResetModifier(); //set the weapon modifier to 0 at the start of game
    }
    
    private void SetShipStats()
    {
        ShipDamageBonus = GameData.gameData.shipDMG;
        health          = GameData.gameData.shipHP;
        moveSpeed       = GameData.gameData.shipSpeed;
    }


    private void SetUpMoveBoundaries()
    //the point 1,0 is the top left of the camera in the wiewport mode
    //1,1 is the top right
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector2(0,0)).x + paddingX;
        xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x -paddingX;
        yMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + paddingY;
        yMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).y -paddingY;
    }
    
    void Update ()
    {
        if (!dying && !PauseMenu.gameIsPaused)
        {
            Move();
            if (Input.GetButton("Fire1"))
                Fire();
        }
    }

    public void SelectWeapon(Weapons newWeapon) //called from WeaponChanger
    {
        switch (newWeapon)
        {
            case Weapons.MachineGun:
                weaponSelected = GetComponent(typeof(MachineGun)) as WeaponClass;
                break;

            case Weapons.Rocket:
                weaponSelected = GetComponent(typeof(Rocket)) as WeaponClass;
                break;

            case Weapons.Laser:
                    weaponSelected = GetComponent(typeof(LaserController)) as WeaponClass; //The laser is in a children Laser Controller (separate object)
                                                                                           //because it needs Line renderer and animator, so its better dont mess the player object 
                break;                                                                  
                
            case Weapons.Ripple:
                weaponSelected = GetComponent(typeof(Ripple)) as WeaponClass;
                break;

            case Weapons.Ball:
                weaponSelected = GetComponent(typeof(Ball)) as WeaponClass;
                break;
        }
        weaponSelected.EquipWeapon(GetWeapon(),weapon_upgrade_lvl);
    }


    public string GetWeapon()
    {

        if (weaponSelected.GetType() == typeof(LaserController))
            return "Laser";
        else if (weaponSelected.GetType() == typeof(MachineGun))
            return "MachineGun";
        else if (weaponSelected.GetType() == typeof(Ball))
            return "Ball";
        else if (weaponSelected.GetType() == typeof(Ripple))
            return "Ripple";
        else if (weaponSelected.GetType() == typeof(Rocket))
            return "Rocket";
        else
        {
            Debug.LogError("weaponSelected type doesnt match ?");
            return null;
        }
    }

    public byte LevelUp()
    {
        ++Level;
        weaponSelected.CheckModifierAndEquip(GetWeapon(),weapon_upgrade_lvl);
        return Level;
    }

    private void Fire()
    {
        weaponSelected.Fire(); //calls Fire through polymorfism
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent(typeof(DamageDealer)) as DamageDealer;
        if (damageDealer)
        {
            (FindObjectOfType(typeof(MusicManager)) as MusicManager).PlayAudio(loseLife, 5);
            damageDealer.Hit();
            if (!dying)
                ProcessHit(damageDealer);
        }
    }
    
    private void ProcessHit(DamageDealer damageDealer)
    {
        health -= (int)damageDealer.Damage;
        panelController.UpdateLife(health);
        if (health <= 0)
        {
            dying = true;
            GetComponent<Animator>().SetTrigger("die");
            AudioSource.PlayClipAtPoint(deathSFX, Camera.main.transform.position, deathSoundVolume);
        }
    }
    public void IncrementLife()
    {
        if (health < 10)
        {
            ++health;
            panelController.UpdateLife(health);
        }
    }

    public void Die()
    {//call from animation die
        FindObjectOfType<LevelManager>().LoadGameOver();
        Destroy(gameObject);
    }

    private void Move()
    {
        mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        mousePosition.x = Mathf.Clamp(mousePosition.x, xMin, xMax);
        mousePosition.y = Mathf.Clamp(mousePosition.y, yMin, yMax);
        transform.position = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
    }
}
