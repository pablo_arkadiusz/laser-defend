﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
  //This class is a little bit different to other Weapon class. It dont inherit from WeaponClass. instead the LaserControler does.
  //And also the laser controller controls the laser behaviour. Because laser has Line renderer for every laser instantiated
  //So the player gameobject cant have 5 Line renderer components.
  //this weapon also dont have cooldown, so it use thesecond variable form XML as Distance and not as Cooldown like the other weapon classes

{ 
    private LineRenderer lineRenderer;
    private GameObject target; //object that is hit by laser
    private float laserDistance; //LASER DISTANCE IS GETTED FROM THE XML SECOND DATA , BECAUSE LASER DONT HACVE COOLDOWN

    /*Good info about ignore layers by Raycast:   https://stackoverflow.com/questions/43559722/using-layers-and-bitmask-with-raycast-in-unity*/

    //layers to be ignored by raycast Laser
    int enemyProyectileLayer;
    int friendProyectileLayer;
    int friendLayer;
    int ignoreRaycastLayer; //For the objects that specified to be in the Ignore Raycast layer 

    int definitiveLayerMask;

    //Damage related variables
    private const float BASE_LASER_DAMAGE = 1f; //laser base damage
    private float definitiveDamage;
    private Coroutine doingDamageCorroutine;
    private bool turnedOn;
    public bool Locked { set; get; } //Some lasers are intantiated but shouldnt turnOn. (The central laser dont turn on when the WeaponModifier is 2)

    private float degreesOfLaser;
    private Vector2 degreesofRaycast;



    void Start()
    {
        //definitiveDamage = BASE_LASER_MULTIPLIER * damageMultiplier;
        lineRenderer = GetComponent(typeof(LineRenderer)) as LineRenderer;
        lineRenderer.enabled = false;
        lineRenderer.useWorldSpace = true;

        //calculate layerMask to ignore the collision between laser and some objects
        CalculateLayerMask();
    }

    //DIRECTION OF RAYCAST ITS CALCULATED FROM THE ORIGIN SPECIFICATED (THE FIRST PARAMETER) (0,1) or 0,100 and actually all (0,positive value) IS GOING TOP (0,-1) IS GOING BOTTOM 
    //IT CAN BE CALCULATED WITH THE SHINUS TEOREM FORMULA
    //It can be done that the program calculate the degrees and the vector2 of that degrees
    //But I made it by hand(calculated in papers) to do it faster
    //This direction Vectors are witten in LaserController as readonly and here are setted with SetDegrees

    private IEnumerator EmittingLaser()                                  
    {
        float distanceOfLineRenderer=0f;
        while (true)
        {
            if (distanceOfLineRenderer < laserDistance)
                distanceOfLineRenderer += 0.1f; //it adds a growing effect to the laser and also to the Raycast

            RaycastHit2D hit = Physics2D.Raycast(transform.position, degreesofRaycast, distanceOfLineRenderer, definitiveLayerMask);
            //Debug.DrawLine(transform.position, hit.point);
            lineRenderer.SetPosition(0, transform.position); //starting position of the laser is always the player position
            if (hit)
            {
                distanceOfLineRenderer = hit.point.y - transform.position.y; //update the distance of line renderer as the distance between the player and the enemy 
                                                                             //to keep growing when it stop hitting the enemy
                lineRenderer.SetPosition(1, hit.point); //if the laser hit something set the top position of the laser to that object
                target = hit.transform.gameObject;
                StartDoingDamage();
            }
            else //if nothing is hit create laser of max distance
            {
                Vector2 endLaserPosition = RotateVector(-degreesOfLaser); //Create a point (of distance 1) with the laser direction based in the degrees of the laser (0,1) if going top (1,0) if right, etc
                endLaserPosition *= distanceOfLineRenderer; //Adds the actual distance to that point
                //Update the laser position based on player position
                endLaserPosition.x += transform.position.x;
                endLaserPosition.y += transform.position.y;

                lineRenderer.SetPosition(1, endLaserPosition); //finally set the laser end distance to the resulting point
                StopDoingDamage();
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void SetDegrees(float laserDegree, Vector2 directionDegree) //should be asigned at instantiation. In LaserController
    {
        degreesOfLaser = laserDegree;
        degreesofRaycast = directionDegree;
    }

    public void Fire()
    {
        if (!turnedOn&&!Locked)
        {
            turnedOn = true;
            lineRenderer.enabled = true;
            StartCoroutine(EmittingLaser());
            StartCoroutine(WaitingToTurnOff());
        }
    }

    private Vector2 RotateVector( float a) //To set the position of the line renderer. Set a angle going top with idstance 1 (0,1) and rotate it the degrees needed
    {                                                      
        a += 90;
        a *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(a), Mathf.Sin(a));
    }

    private IEnumerator WaitingToTurnOff() //This method is responsible for turning off the laser.
    {
        while (Input.GetButton("Fire1")&&!Locked)
            yield return new WaitForEndOfFrame();
        (transform.parent.GetComponent(typeof(LaserController)) as LaserController).StopLaserSound();
        TurnOff();
    }

    public void TurnOff()
    {
        lineRenderer.enabled = false;
        StopAllCoroutines();
        turnedOn = false;
    }

    private IEnumerator DoDamage()
    {
        if (!target)
        {
            Debug.LogError("Doing damage to non target ?");
            yield return null;
        }
        else if (!target.GetComponent(typeof(NonFriendly))) Debug.LogError("Target is not an enemy or asteroid ??");
        while (true)
        {
            (target.GetComponent(typeof(NonFriendly)) as NonFriendly).ProcessHit(definitiveDamage);
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void StartDoingDamage()
    {
        if (doingDamageCorroutine == null)
            doingDamageCorroutine = StartCoroutine(DoDamage());
    }

    private void StopDoingDamage()
    {
        if (doingDamageCorroutine != null)
        {
            StopCoroutine(doingDamageCorroutine);
            doingDamageCorroutine = null;
        }
    }

    public void SetStat(float damage,float distance)
    {
        laserDistance = distance;
        definitiveDamage = damage;
    }


    private void CalculateLayerMask()
    {
        //find layers
        enemyProyectileLayer  = LayerMask.NameToLayer("EnemyProyectile");
        friendProyectileLayer = LayerMask.NameToLayer("FriendProyectile");
        friendLayer           = LayerMask.NameToLayer("Friend");
        ignoreRaycastLayer    = LayerMask.NameToLayer("Ignore Raycast");

        //Calculate layermask to Raycast to. (Ignore "EnemyProyectile" && "FriendProyectile" && "Friend" layers)
        definitiveLayerMask = ~((1 << enemyProyectileLayer) | (1 << friendProyectileLayer) | (1 << friendLayer) | (1<<ignoreRaycastLayer));
    }
}
