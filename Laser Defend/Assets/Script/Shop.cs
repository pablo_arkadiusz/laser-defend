﻿using System.Xml;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
public class Shop : MonoBehaviour
{
    private enum Positions:byte { Left=1, Center, Right };
    [SerializeField] private Text txtTitle, txtStats, txtInfo ,txtPrice, txtMyMoney;
    [SerializeField] private Button btnAvance, btnBackOff, buyButton;
    [SerializeField] private Transform shopContainer;
    [SerializeField] private Animator auxiliarPanel;
    [SerializeField] private ScrollRectWithoutDrag scrollRect;
    [SerializeField] private Hangar hangar;

    private Positions actualPos = Positions.Left;

    private Color targetColor = new Color(10, 255, 0);
    private Image actualTarget;
    private byte targetID;
    private bool auxiliarPanelOn;

    private void Start()
    {
        //DISABLE BUTTONS AND ITEM THAT PLAYER ALREADY POSSES FROM THE SHOP
        byte index = 1;
        foreach (Transform shopLevel in shopContainer)
        {
            Button[] shipButtons = shopLevel.GetComponentsInChildren<Button>();
            foreach (Button shipButton in shipButtons)
            {
                if (GameData.gameData.shipsInPosesion[index])
                {
                    shipButton.interactable = false;
                    Image shipImage = shipButton.transform.GetChild(0).GetComponent(typeof(Image)) as Image;
                    shipImage.sprite = Resources.Load<Sprite>("LoadingImage");
                    shipImage.SetNativeSize();
                }
                ++index;
            } 
        }

        UpdateCoinText();

        btnBackOff.interactable = false;
        UpdateText();
    }

    public void AvancePosition() //called from button. avence the position of the shop to next generation
    {
        if (!auxiliarPanelOn)
        {
            ++actualPos;
            UpdateText();

            btnBackOff.interactable = true;
            if (actualPos == Positions.Right)
                btnAvance.interactable = false;

            ResetTarget();
            buyButton.interactable = false;
        }
    }

    public void BackOffPosition() //called from button. Back off the position of the shop to previous generation
    {
        if (!auxiliarPanelOn)
        {
            --actualPos;
            UpdateText();

            btnAvance.interactable = true;
            if (actualPos == Positions.Left)
                btnBackOff.interactable = false;
            ResetTarget();
            buyButton.interactable = false;
        }
    }

    private void UpdateText() //update the title, info and price texts
    {
        switch (actualPos)
        {
   
            case Positions.Left:
                txtTitle.text = "Generacion 3";
                txtInfo.text = "Completely autonomous and self-propelled ships by a mixed chemical-nuclear engine. They are of the third generation, but there is no doubt" +
                    " that their design, aerodynamically effective " +
                    "together with a lavish exterior color, made them one of the most-known icons of space";

                txtPrice.text = "10";
                return;

            case Positions.Center:
                txtTitle.text = "Generacion 2";
                txtInfo.text = "They stand out for their ingenious system of rotating lower propellers that allow the ship to maneuver in atmosphere as well as land and " +
                    "take off from planetoids. They have one of the most beautiful and devastating designs that has been seen in years.";
                txtPrice.text = "20";
                return;

            case Positions.Right:
                txtTitle.text = "Generacion 1";
                txtInfo.text = "The fastest scrap heap in the galaxy. So fast that he was able to do the Kessel Race in less than 12 parsecs." +
                    " which is extraordinarily fast, since the parsec is a measure of distance and not of time.";
                txtPrice.text = "30";
                return;
        }
    }

    private void UpdateCoinText() //update text with actual player money
    {
        txtMyMoney.text = GameData.gameData.money.ToString();
    }

    public void TargetThisButton(Button button) //change the color fo the selected ship, find that ship and updates the text stats to that ship
    {
        if (!auxiliarPanelOn)
        {
            if (actualTarget != null)
            actualTarget.color = Color.white;

            actualTarget = button.GetComponent(typeof(Image)) as Image;
            actualTarget.color = targetColor;

            byte shipNum = byte.Parse(Regex.Match(button.name, @"\d+").Value); //extract the number from the string name of the button
            targetID = shipNum;
            UpdateStatsTextFromXML();

            buyButton.interactable = true;
        }
    }

    private void ResetTarget()
    {
        if (actualTarget)
        {
            actualTarget.color = Color.white;
            actualTarget = null;
            txtStats.text = "";
        }
    }

    private void UpdateStatsTextFromXML()
    {
        string strDamage, strHealth, strMoveSpeed;

        TextAsset textAsset = (TextAsset)Resources.Load("ShipStats.xml");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(textAsset.text);

        string xmlPathPattern = "/Document/ship" + targetID + "/Damage";
        XmlNode node = xmlDoc.SelectSingleNode(xmlPathPattern);
        strDamage = node.InnerXml;
        strHealth = node.NextSibling.InnerXml;
        strMoveSpeed = node.NextSibling.NextSibling.InnerXml;

        txtStats.text =
            "DAMAGE: " + strDamage + "\n\n" +
            "HEALTH: " + strHealth + "\n\n" +
            "MOVE SPEED: " + strMoveSpeed;
    }

    public void ConfirmPurchase()
    {

        if (GameData.gameData.money >= int.Parse(txtPrice.text))
        {
            buyButton.interactable = false; //necesary, with this player can buy without money if he click very fast second time another target
            auxiliarPanelOn = true;
            auxiliarPanel.SetTrigger("entry");
            DisableElementsOnBuy();
        }
        else //not enough money
            (txtPrice.GetComponent(typeof(Animator)) as Animator).SetTrigger("makeBig");
        
    }

    public void BuyTargetShip()
    {
        auxiliarPanel.SetTrigger("leave");
        MakeTargetShipInvisible(targetID);
        SendTargetToHangar();
        GameData.gameData.shipsInPosesion[targetID] = true;
        GameData.gameData.money -= uint.Parse(txtPrice.text);
        GameData.SaveData();
        UpdateCoinText();
        ResetTarget();
        auxiliarPanelOn = false;
        scrollRect.enabled = true;
        if(actualPos != Positions.Right)
            btnAvance.interactable = true;
        if (actualPos != Positions.Left)
            btnBackOff.interactable = true;
        buyButton.interactable = false;
    }
    public void CancelPurchase()
    {
        auxiliarPanel.SetTrigger("leave");
        auxiliarPanelOn = false;
        EnableElementsAfterBuy();
        buyButton.interactable = true;
    }

    private void MakeTargetShipInvisible(byte shipID)
    {
        string shipPath = "ShopLevel" + (byte)actualPos + "/ship"+ shipID + "btn/nave "+ shipID;

        Transform shopShip = shopContainer.Find(shipPath);
        //buttonImage.color = new Color(255, 255, 255, 0);
        shopShip.gameObject.SetActive(false);
        (shopShip.transform.parent.GetComponent(typeof(Button)) as Button).interactable = false;
    }

   private void DisableElementsOnBuy()
    {
        scrollRect.enabled = false;
        btnAvance.interactable = false;
        btnBackOff.interactable = false;
    }
    private void EnableElementsAfterBuy()
    {
        scrollRect.enabled = true;
        if (actualPos != Positions.Right)
            btnAvance.interactable = true;
        if (actualPos != Positions.Left)
            btnBackOff.interactable = true;
    }

    private void SendTargetToHangar()
    {
        hangar.AddShip(targetID);
    }
}


