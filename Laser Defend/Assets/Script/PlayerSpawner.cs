﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour //just instantiate the ship that player has selected
{
    private void Start()
    {
        Instantiate(Resources.Load("PlayerShip/player " + GameData.gameData.selectedShipID));
    }
}
