﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class Hangar : MonoBehaviour
    //Hangar manages the ships that you actually posseses
{
    [SerializeField] private Transform container; //the container of ships in your possesion
    [SerializeField] private ScrollSnapRect scrollSnapRect;
    [SerializeField] private Button btnBackOff, btnAvance;
    [SerializeField] private Text txtStats;
    [SerializeField] private AnimationManager animationManager;

    private List<byte> shipIDList = new List<byte>();
    private byte currentPos;

    private void Start()
    {
        currentPos = GameData.gameData.shipHangarPosition = 0;

        //Adds the owned ships to Hangar
        GameData.gameData.shipsInPosesion[0] = true; //the ship0 is the starting ship so player always owns it
        shipIDList.Add(0);
        for (byte i=1; i < GameData.gameData.shipsInPosesion.Length; ++i)
            if (GameData.gameData.shipsInPosesion[i])
                AddShip(i);


        //Find selectedShip position, it can change because (when player buy new ships the last bought ships are the last in hangar.
        //But when player entry again to the Hangar scene they are ordered by te ship ID
        string path = "ship" + GameData.gameData.selectedShipID + "(Clone)";
        Transform child = container.Find(path);
        int shipIndex = child.GetSiblingIndex(); //Get the index of the child of the previous ship (it could have changed because it is ordered by the ship ID if the player entry to the hangar scene)

        //Avance the page to the selectedShip
        container.parent.GetComponent<ScrollSnapRect>().LerpToPage(shipIndex);
        for (byte i = 0; i < shipIndex; ++i) //also actualice the buttons interactable state and other the hangar configurations
            AvancePosition();

        if (currentPos==0)
            btnBackOff.interactable = false;
        if (shipIDList.Count == 1)
            btnAvance.interactable = false;
        
        UpdateStats(); //update texts to match actual ship stats
        ChangeRotationTarget(); //the selected ship rotate
    }

    public void AddShip(byte shipID)
    {
        GameObject instance = Instantiate(Resources.Load("ShipHangar/ship" + shipID)) as GameObject;
        instance.transform.SetParent(container);
        scrollSnapRect.UpdateComponents();
        shipIDList.Add(shipID);
        btnAvance.interactable = true;
    }

    public void AvancePosition() //called through hangar button and also in the Start function to adjust the position to actual ship
    {
        ++currentPos;
        GameData.gameData.shipHangarPosition = currentPos;
        GameData.gameData.selectedShipID = shipIDList[currentPos];
        GameData.SaveData();
        ChangeRotationTarget();
        UpdateStats();
        btnBackOff.interactable = true;
       
        if (currentPos == shipIDList.Count - 1)
            btnAvance.interactable = false;
    }

    public void BackOffPosition() //called through hangar button
    {
        --currentPos;
        GameData.gameData.shipHangarPosition = currentPos;
        GameData.gameData.selectedShipID = shipIDList[currentPos];
        GameData.SaveData();
        ChangeRotationTarget();
        UpdateStats();

        btnAvance.interactable = true;
        if (currentPos == 0)
            btnBackOff.interactable = false;
    }

    public void UpdateStats()
        //Update text to match with the selected ship stats. Read data from XML document
    {
        float dmg, moveSpeed; 
        byte hp;
         
        TextAsset textAsset = (TextAsset)Resources.Load("ShipStats.xml");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(textAsset.text);
        string xmlPathPattern = "/Document/ship" + shipIDList[currentPos] + "/Damage";
        XmlNode node = xmlDoc.SelectSingleNode(xmlPathPattern);
        
        dmg = float.Parse(node.InnerXml);
        hp = byte.Parse(node.NextSibling.InnerXml);
        moveSpeed = float.Parse(node.NextSibling.NextSibling.InnerXml);

        txtStats.text =
            "DAMAGE: " + dmg + "\n\n" +
            "HEALTH: " + hp + "\n\n" +
            "MOVE SPEED: " + moveSpeed;

        GameData.gameData.shipHP = hp;
        GameData.gameData.shipDMG = dmg;
        GameData.gameData.shipSpeed = moveSpeed;
        GameData.SaveData();
    }

    private void ChangeRotationTarget()
    {
        Transform actualShip = container.GetChild(currentPos).GetChild(0);
        animationManager.ChangeSlowlyRotatedObj(actualShip);
    }
 
}

