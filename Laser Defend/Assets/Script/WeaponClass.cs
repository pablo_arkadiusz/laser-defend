﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

// ABSTRACT CLASS
// ALL PLAYER WEAPONS INHERIT FROM THIS CLASS AND FIRE THROUGH POLIMORFIC VARIABLE.
public abstract class WeaponClass : MonoBehaviour
{
    private const byte NUMBER_OF_MODIFIERS=5;
    private const byte NUMBER_OF_UPGRADES= 4;

    public abstract void Fire();
    protected abstract float GetBaseDamage();

    protected float damageMultiplier;
    protected float secondAttribute;
    protected bool isOnCooldown;

    static private byte weaponModifier=1; //A weapon modifier is a change in the behaviour of each weapon. Like shoot 1 bullet, shoot 2 bullet and shoot 3 bullets
                                          //Each weaponModifier has 4 upgrades, a upgrade improve damage and cooldown. The upgrade variables are read from XML document

  
 
    protected byte WeaponModifier
    {
        private set{ weaponModifier = value > NUMBER_OF_MODIFIERS ? NUMBER_OF_MODIFIERS : value; } //There are only 5 modifiers on each weapon
        get{ return weaponModifier; }
    }

    private void IncrementModifier() {++WeaponModifier;}

    private void ReadData( string weapon, byte currentUpgrade) //read variables from XML document
    {
        TextAsset textAsset = (TextAsset)Resources.Load("WeaponData.xml");
 
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(textAsset.text);
       
        string xmlPathPattern = "/Weapon/" + weapon + "/upgrade_" + currentUpgrade + "/damageMultiplier";
 
        XmlNode node= xmlDoc.SelectSingleNode(xmlPathPattern);

        damageMultiplier = float.Parse(node.InnerXml);
        secondAttribute     = float.Parse(node.NextSibling.InnerXml);
    }

    private byte CalculateCurrentUpgrade(byte level)
    {
        //if it's a multiple of 4 (when leven %4==0) returns 4 (in levels 4,8,12,16 and 20)
        //else returns the remainder of level/4. Example: level 5 returns 1, level 6 return 2, level 7 returns 3
        return (byte)(((level % NUMBER_OF_UPGRADES) == 0) ? NUMBER_OF_UPGRADES : (level % NUMBER_OF_UPGRADES));                                                        
    }

    public void CheckModifierAndEquip(string weapon, byte newLevel)
    //Checks the modifiers and gets the data from the XML, its used when player level ups. 
    //But may not be used when player gets a WeaponChanger, because they dont increment the modifiers. Instead they use EquipWeapon
    {
        if ((newLevel - 1) % NUMBER_OF_UPGRADES == 0 && newLevel!=1) // if the previous level is multiple of 4 then increment modifier (on levels 5,9,13 and 17)
            IncrementModifier();
        
        EquipWeapon(weapon,newLevel);
    }

    public void EquipWeapon(string weapon, byte level)
    {
        byte currentUpgrade = CalculateCurrentUpgrade(level);
        ReadData(weapon, currentUpgrade);
    }

    private void SetBulletDamage(DamageDealer bullet) 
    {
        float shipMultiplier = (GetComponent(typeof(Player)) as Player).ShipDamageBonus;
        float totalDamage = shipMultiplier * GetBaseDamage() * damageMultiplier;
        bullet.Damage = totalDamage;
    }

    protected GameObject CreateCentralBullet(GameObject bulletPrefab,float bulletSpeed=0)
    {
        Vector2 posInicio = transform.position + new Vector3(0, 0.4f);
        GameObject bullet = Instantiate(bulletPrefab, posInicio, Quaternion.identity); //Quaternion.identity maintains the rotation unchanged
        SetBulletDamage(bullet.GetComponent(typeof(DamageDealer)) as DamageDealer);
        (bullet.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D).velocity = new Vector2(0, bulletSpeed);
        return bullet;
    }

    protected void CreateTwoSimetricalBullets(GameObject bulletPrefab, float spawnXDistance, float spawnYDistance = 0, float rotation=0, float bulletSpeed=0)
    {
        Vector2 spawnPos = new Vector2(spawnXDistance, 0);
        for (byte i = 0; i < 2; ++i)
        {
            rotation *= -1f;
            Vector2 posInicio = transform.position + new Vector3(0, 0.4f + spawnYDistance);
            spawnPos *= -1 * (Vector2.one);
            posInicio += spawnPos;
            GameObject bullet = Instantiate(bulletPrefab, posInicio, Quaternion.identity); //Quaternion.identity maintains the rotation unchanged

            (bullet.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D).velocity = new Vector2(rotation, bulletSpeed);
            bullet.transform.Rotate(0, 0, -rotation);
            SetBulletDamage(bullet.GetComponent(typeof(DamageDealer)) as DamageDealer);
        }
    }
    public void ResetModifier()
    {
        weaponModifier = 1;
    }

}


