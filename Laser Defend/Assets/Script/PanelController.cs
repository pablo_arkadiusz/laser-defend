﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using TMPro;

public class PanelController : MonoBehaviour
    //THIS SCRIPT MANAGE ALL THE PANELS IN THE GAME_SCENE, SO ITS PRETTY LARGE
{
    [Header("Wave Panel")]
    [SerializeField] private GameObject wavePanel;
    [SerializeField] private Text waveText;
    [SerializeField] private Text waveInfoText;
    private Animator wavePanelAnimator;

    [Header("Points Panel")]
    [SerializeField] private TextMeshProUGUI pointsText;
    [SerializeField] private ScoreManager scoreManager;

    [Header("General Panel ")]
    [SerializeField] private GameObject generalPanel;
    [SerializeField] private Image weaponImg;
    [SerializeField] private Transform lifes;
    private byte actualLifes=5; //actual lifes
    [SerializeField] private Text upgradeLevelTxt;
    [SerializeField] private Text generalPanelWaveTxt;

    [Header("Weapon Icons")]
    [SerializeField] private Sprite laserSprite;
    [SerializeField] private Sprite ballSprite;
    [SerializeField] private Sprite rippleSprite;
    [SerializeField] private Sprite machineGunSprite;
    [SerializeField] private Sprite rocketSprite;
    private bool notDisable = false;

    [Header("Last Level Panel")]
    [SerializeField] private Animator lastPanelAnimator;
    [SerializeField] private Text txtCalcReward, txtReward;
    [SerializeField] private Image fillImage;
    [SerializeField] private Button btnOK;
    [SerializeField] private Image blackImg;
    private float fillTime = 3.5f;
    private float reward;

    [Header("Sounds")]
    [SerializeField] private AudioClip loginMessaje;
    [SerializeField] private AudioClip endMessaje1;
    [SerializeField] private AudioClip endMessaje2;


    private void Awake()
    {
        wavePanelAnimator = wavePanel.GetComponent(typeof(Animator)) as Animator;
        UpdatePointText();
        ChangeWeaponPanelImg(GetWeaponImg(GameData.gameData.weaponSelected)); //update the weapon image of the weaponPanel
    }

    private void Start()
    {
        Cursor.visible = false;
        blackImg.color = Color.black;
        StartCoroutine(StartEffect());
    }

    private void Update()
    {
        foreach (Transform life in lifes)
            life.transform.GetChild(0).Rotate(0, 4, 0); //rotate all life images
        weaponImg.transform.Rotate(0, 0, -4); //rotate weapon image
    }


    private void UpdateLifesIMG(byte newLife)  //this method update the lifes images. the max life is 10 so 5 lifes are represented as red hearts, and they grow from left to right
                                               //and the next 5 lifes are represented as black lifes. They replace the red ones growing from right to left. So 5 black lifes is the max_life (10)
                                               //It should be optimiced because was wrote in "fast mode"  
    {
        if (newLife == actualLifes) return;
        const byte MAX_LIFES_DISPLAYED = 5;
        byte actualBlackLifes = (actualLifes > MAX_LIFES_DISPLAYED) ? (byte)(actualLifes - MAX_LIFES_DISPLAYED) : (byte)0;

        Image[] lifesArray = lifes.GetComponentsInChildren<Image>();


        if (newLife > actualLifes) //Adding lifes
        {
            byte maxLifesToAdd = (byte)((MAX_LIFES_DISPLAYED * 2) - actualLifes);
            byte lifesToAdd = (byte)Mathf.Clamp((newLife - actualLifes), 0, maxLifesToAdd);

            byte normalLifesToAdd;
            byte blackLifesToAdd;

            if (lifesToAdd > MAX_LIFES_DISPLAYED - actualLifes) 
            {
                normalLifesToAdd = (byte)Mathf.Clamp((MAX_LIFES_DISPLAYED - actualLifes), 0, 5);
                blackLifesToAdd = (byte)Mathf.Clamp((lifesToAdd - normalLifesToAdd), 0, 5);
            }
            else
            {
                normalLifesToAdd = lifesToAdd;
                blackLifesToAdd = 0;
            }

            //ADDING NORMAL LIFES
            byte index = (actualLifes);

            for (; index < normalLifesToAdd + actualLifes; ++index)
            {
                lifesArray[index].enabled = true;
                lifesArray[index].GetComponentInParent<Animator>().SetTrigger("Appear");
                StartCoroutine(SetNotDisableTrue());
            }

            //ADDING BLACK LIFES
            byte indexToPrintBlack = (byte)(MAX_LIFES_DISPLAYED - (actualBlackLifes + 1));
            for (byte i = 0; i < blackLifesToAdd; --indexToPrintBlack, ++i)   //creating black lifes     
            {
                lifesArray[indexToPrintBlack].GetComponentInParent<Animator>().SetTrigger("Appear");
                lifesArray[indexToPrintBlack].color = Color.black;
                StartCoroutine(SetNotDisableTrue());
            }
            actualBlackLifes += blackLifesToAdd;
            actualLifes += lifesToAdd;
        }
        
        if (newLife < actualLifes) //substracting lifes
        {
            byte lifesToSubstract = (byte)(actualLifes - newLife);
            //substracting black lifes first, later substract normal life
            sbyte remainingBlackLifes = (sbyte)(actualBlackLifes - lifesToSubstract);
            byte blackLivesToSubstract;
            if (lifesToSubstract > actualBlackLifes) blackLivesToSubstract = actualBlackLifes;
            else blackLivesToSubstract = lifesToSubstract;
           
            byte indexToSubstractBlack = (byte)(MAX_LIFES_DISPLAYED - actualBlackLifes);
            if (actualBlackLifes > 0)
                for (byte i = indexToSubstractBlack; i < indexToSubstractBlack+blackLivesToSubstract; ++i) 
                {
                    lifesArray[i].GetComponentInParent<Animator>().SetTrigger("LoseBlack");
                    lifesArray[i].color = Color.white;
                }
            if (remainingBlackLifes < 0)  //substracting normal lifes
            {
                byte normalLifesToSubstract = (byte)Mathf.Abs(remainingBlackLifes);
                sbyte indexStart = (sbyte)((actualLifes-actualBlackLifes) - 1);
                sbyte limit = (sbyte)(indexStart-normalLifesToSubstract);

                for (sbyte i = indexStart; i > limit; --i)
                {
                    lifesArray[i].GetComponentInParent<Animator>().SetTrigger("Disappear");
                    //lifesArray[i].enabled = false;
                    StartCoroutine(DisableImageInSec(lifesArray[i]));
                }
            }
            actualLifes -= lifesToSubstract;
            actualBlackLifes = (byte)Mathf.Clamp(remainingBlackLifes, 0, 1);
        }
    }

    public void UpdateLife(int life) //called when player get new life or has been hitted
    {
        if (life < 0) life = 0;
        UpdateLifesIMG((byte)life);
    }

    public void UpgradeLeveLTxt(byte level)
    {
        upgradeLevelTxt.text = level.ToString(); 
    }

    private IEnumerator RotateImage()
    {
        do
        {
            weaponImg.transform.Rotate(0, 0, -4);
            yield return new WaitForEndOfFrame();

        } while (true);
    }

    public void ChangeWeaponPanelImg(Sprite img)
    {
        weaponImg.sprite = img;
    }

    public Sprite GetWeaponImg(Weapons weaponType) //Called when player get new weapon to change the weapon sprite in the general panel
    {
        switch(weaponType)
        {
            case Weapons.MachineGun:
                return machineGunSprite;
            case Weapons.Ball:
                return ballSprite;
            case Weapons.Laser:
                return laserSprite;
            case Weapons.Ripple:
                return rippleSprite;
            case Weapons.Rocket:
                return rocketSprite;
            default:
                return null;
        }
    }

    public void UpdatePointText()
    {
        pointsText.text = scoreManager.GetPoints().ToString();
    }

    public void AppearWavePanel() //at begin of every new wave this panel show the wave number and a message read from xml
    {
        wavePanelAnimator.SetTrigger("Appear");
    }
    public void DisappearWavePanel()
    {
        wavePanelAnimator.SetTrigger("Disappear");
    }

    public void UpdateWaveTexts(byte actualWave) //update the wave message from xml
    {
        waveText.text = actualWave.ToString();
        waveInfoText.text = ReadDate(actualWave);
        generalPanelWaveTxt.text = actualWave.ToString();
    }

    private IEnumerator DisableImageInSec(Image img)
    {
        yield return new WaitForSeconds(1f);
        if (!notDisable)
            img.enabled = false;
    }

    private IEnumerator SetNotDisableTrue()
    {
        notDisable = true;
       // When notDisable is true it will not disable the hearth (it is used when player is damaged but in at the same time get a life_bonus. )
        yield return new WaitForSeconds(1.5f);
        notDisable = false;
    }

    private string ReadDate(uint actualWave) //Reads from XML
    {
        TextAsset textAsset = (TextAsset)Resources.Load("WaveText.xml");

        XmlDocument xmlDoc = new XmlDocument();

        xmlDoc.LoadXml(textAsset.text);

        string xmlPathPattern = "/Worlds/World_" + WaveManager.GetActualWorld() + "/Wave_" + actualWave;
        XmlNode node = xmlDoc.SelectSingleNode(xmlPathPattern);

        return node.InnerXml;
    }


    public void ShowLastLevelPanel()
    {
        btnOK.interactable = false;
        txtReward.gameObject.SetActive(false);
        txtCalcReward.color = new Color(255, 255, 255, 0);
        lastPanelAnimator.SetTrigger("entry");
        Cursor.visible = true;
    }

    public void StartCalculateReward()
    {
        StartCoroutine(ShowCalculateText());
        StartCoroutine(FillImage());
        StartCoroutine(SubstractingPoints());
        CalculateReward();
        musicManager.PlayAudio(endMessaje1, 4);
    }
    private IEnumerator ShowCalculateText()
    {
        Color moreAlpha = new Color(0, 0, 0, 0.05f);
        while(txtCalcReward.color.a<1)
        {
            txtCalcReward.color += moreAlpha;
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator FillImage()
    {
        float timeToWait = 0.05f;
        float fillValue = timeToWait / fillTime;
        while(fillImage.fillAmount<1)
        {
            fillImage.fillAmount += fillValue;
            yield return new WaitForSeconds(timeToWait);
        }
        txtReward.gameObject.SetActive(true);
        txtReward.text += "\n " + reward +" Coins";
        btnOK.interactable = true;
    }

    private void CalculateReward()
    {
        reward = scoreManager.GetPoints() / 500;
        GameData.gameData.money += (uint)reward;
        GameData.SaveData();
    }

    private IEnumerator SubstractingPoints()
    {
        float timeToWait = fillTime / scoreManager.GetPoints() * 200;
        Debug.Log("time:" + timeToWait);
        while (scoreManager.GetPoints()>=0)
        {
            scoreManager.SubstractScore(200);
            yield return new WaitForSeconds(timeToWait);
        }
    }

    private MusicManager musicManager;

    private IEnumerator StartEffect()
    {   
        musicManager = FindObjectOfType(typeof(MusicManager)) as MusicManager;
        Debug.Log(musicManager.gameObject.name);
        musicManager.PlayAudio(loginMessaje,1);
        yield return new WaitForSeconds(loginMessaje.length);
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeOut()
    {
        Color newColor = new Color(0, 0, 0, 0.03f);
        while (blackImg.color.a >= 0)
        {
            blackImg.color -= newColor;
            yield return new WaitForEndOfFrame();
        }
        musicManager.StartGameAudio();
    }

    public void FadeAndLoad()
    {
        StartCoroutine(RoutineFadeAndLoad());
    }

    private IEnumerator RoutineFadeAndLoad()
    {
        Color newColor = new Color(0, 0, 0, 0.05f);
        while (blackImg.color.a < 1)
        {
            blackImg.color += newColor;
            yield return new WaitForEndOfFrame();
        }
        musicManager.PlayAudio(endMessaje2, 4);
        yield return new WaitForSeconds(endMessaje2.length);
        (FindObjectOfType(typeof(LevelManager)) as LevelManager).LoadStartMenu();
    }



}
