﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    private PanelController panelController;
    private int score;
    private void Start()
    {
        panelController = FindObjectOfType(typeof(PanelController)) as PanelController;
    }

    public void AddScore(int points)
    {
        score += points;
        panelController.UpdatePointText();

    }
    public void SubstractScore(int point)
    {

        if (score - point > 0)
        {
            score -= point;
            panelController.UpdatePointText();
        }
        else
        {
            score = 0;
            panelController.UpdatePointText();
        }
    }
 
    public int GetPoints()
    {
        return score;
    }

    public void ResetScore()
    {
        score = 0;
        panelController.UpdatePointText();
    }
}
