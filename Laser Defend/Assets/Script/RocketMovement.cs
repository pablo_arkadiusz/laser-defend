﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketMovement : MonoBehaviour //ROCKET MOVEMENT FOLLOWS A RANDOM ZIG-ZAG DIRECTION
{

	
	void Start ()
    {
        StartCoroutine(MoverEmZigZag());
	}

    private IEnumerator MoverEmZigZag()
    {
        float secondsToChange = Random.Range(0.2f, 2f);

        float startTime = Time.time;
        float rotationSpeed = 2f;
        float slowSecond = 0.7f;

        if (Random.value > 0.5f) rotationSpeed *= -1;


        do //AT START ROCKET AVANCE SLOWLY
        {
            transform.Translate(Vector3.up * Time.deltaTime * 0.5f);
            yield return new WaitForEndOfFrame();
        } while (Time.time - startTime <= slowSecond);

        do //THEN ROCKET ROTATE, ALSO SLOWLY
        {
            transform.Rotate(0, 0, rotationSpeed);
            transform.Translate(Vector3.up * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        } while ((Time.time - slowSecond) - startTime <= secondsToChange / 2);

        startTime = Time.time;
        rotationSpeed *= -1;

        while (true) //NOW ROCKET GOES FAST IN ZIG ZAG MOVEMENT
        {
            do
            {
                transform.Rotate(0, 0, rotationSpeed);
                transform.Translate(Vector3.up * Time.deltaTime * 5f);
                yield return new WaitForEndOfFrame();
            } while (Time.time - startTime <= secondsToChange);

            startTime = Time.time;
            rotationSpeed *= -1;
        }
    }
}
