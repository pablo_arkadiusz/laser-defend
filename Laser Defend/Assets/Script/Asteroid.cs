﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathFollow))]
[RequireComponent(typeof(BoxCollider2D))]

public class Asteroid : NonFriendly
    //Asteroids should die at end of their paths. (Should be seeted in WaveConfigs)
{
    [SerializeField] private int pointsForKill;
    [SerializeField] private float health = 10;
    [SerializeField] private int damage;
    [SerializeField] private AudioClip deathSFX;

    private float speed;
    private ScoreManager scoreManager;

    private void Start()
    {
        scoreManager = FindObjectOfType(typeof(ScoreManager)) as ScoreManager;
        (GetComponent(typeof(DamageDealer)) as DamageDealer).Damage = damage; //damage of contact with player
    }


    public override void SetConfig(WaveConfig waveConfig)
    {
        speed = waveConfig.GetMoveSpeed();
        SetPathToFollow(waveConfig.GetPathToFollow());
        (GetComponent(typeof(PathFollow)) as PathFollow).destroyAtEndOfPAth = waveConfig.GetDestroyAtEndOfPath(); //Generally it should be true for asteroids
    }

    protected override void SetPathToFollow(Path path)
    {
        PathFollow pathFollow = GetComponent(typeof(PathFollow)) as PathFollow;
        pathFollow.PathToFollow = path;
        pathFollow.speed = speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Actually it only collide with player
        DamageDealer damageDealer = other.gameObject.GetComponent(typeof(DamageDealer)) as DamageDealer;
        if (damageDealer)
        {
            damageDealer.Hit();
            ProcessHit(damageDealer.Damage);
        }
    }

    public override void Die()
    {//called from animator after the animation of death
        scoreManager.AddScore(pointsForKill);
        (FindObjectOfType(typeof(WaveManager)) as WaveManager).EnemyDeath();
        Destroy(gameObject);
    }

    public override void ProcessHit(float dmg)
    {
        health -= dmg;
        if (health <= 0)
            GetComponent<Animator>().SetTrigger("die");
    }
    


}
