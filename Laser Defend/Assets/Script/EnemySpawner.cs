﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private WaveManager waveManager;

    private List<WaveConfig> waveList = new List<WaveConfig>(); //Waves with configurations like speed, paths, number of enemies, etc it is added through waveManager from ResourcesFiles
    private Coroutine[] spawningWaveCoroutine; //A coroutine array to control every "wave formation" of the actual level
    private float maxTimeLimit=0;
    private bool[] waveSpawned;

    private void UpdateWaves()
    {
        maxTimeLimit = 0;
        waveList.Clear(); //Clear the old wave list (waves from previous level)
        WaveConfig[] waveArray = waveManager.GetWaves(); //Get the waves of the actual level
        foreach (WaveConfig wave in waveArray) //Add all the actual waves to the waveList
            waveList.Add(wave);
        spawningWaveCoroutine = new Coroutine[waveList.Count]; //Creates a coroutine for each waves in actual level to control all of them. 
                                                               //Some waves have time limit so after the time they are stopped via this coroutine (usually only Asteroids)
        waveSpawned = new bool[waveList.Count]; // To see if all waves of the level spawned or not. Because the level increase if the enemies count is 0, but only if all waves spawned
                                                // Because there can be some enemies left that still didnt spawned
    }       

    public void SpawnAllWaves()
    {
        UpdateWaves(); //Update the waves to the actual level
        for (int i = 0; i < waveList.Count; ++i) //for every wave in the actual level
        {
            spawningWaveCoroutine[i]= StartCoroutine(SpawnAllEnemiesInWave(waveList[i],i)); // Start Spawning enemies of the actual wave (each wave has different configs (number of enemies,speed,paths,etc))
            if (waveList[i].GetMaxTimeSpawning() > 0) //if the waveconfig has maxTime it will stop spawns after that time even if there are still enemies left to spawn
            {
                float timeLimit = waveList[i].GetMaxTimeSpawning();
                StartCoroutine(StopAfterTimeLimit(timeLimit, i));
                ActualiceMaxTimeLimit(timeLimit);
            }
        }
        //notify the end of waves in the actual level. It still dont increase the level if there are enemies alive. 
        //It will ckeck the end of wave every sec, but after the maxTimeLimit of the waves in the actual level passes

       StartCoroutine(NotifyEndOfWaveAfterTime(maxTimeLimit));
    }

    private IEnumerator CheckIfAllSpawnsEndedEverySec() //Check if all waves spawned, and if they did calls  NotifyEndOfWave();
    {
        bool allWavesEnded;
        do
        {
            allWavesEnded = true;
            foreach (bool waveEnd in waveSpawned)
            {
                if (!waveEnd)
                {
                    allWavesEnded = false;
                    break;
                }
            }
            if (allWavesEnded)
            {
                NotifyEndOfWave();
                yield break;
            }
            yield return new WaitForSeconds(1);
        } while (true);
    }

    private IEnumerator NotifyEndOfWaveAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        if (maxTimeLimit > 0)
            NotifyEndOfWave();
        else
            StartCoroutine(CheckIfAllSpawnsEndedEverySec());

    }

    private void NotifyEndOfWave() //if waveEnded is true and enemies left are 0 the level will increase
    {
        waveManager.waveEnded = true;
        waveManager.CheckIfNoEnemiesLeft();
    }

    private IEnumerator StopAfterTimeLimit(float time,int index) //stop the spawning in the wave of the selected index
    {
        yield return new WaitForSeconds(time);
        StopCoroutine(spawningWaveCoroutine[index]);
    }

    private IEnumerator SpawnAllEnemiesInWave(WaveConfig wave,int numOfWave)
    {
        int enemiesNum = wave.GetNumberOfEnemies();
        float randomFactor;
        float randomNumber;
        float timeBetweenSpawn;
        for (int enemyCount = 0; enemyCount < enemiesNum; ++enemyCount)
        { 
            
            randomFactor = wave.GetSpawnRandomFactor();
            randomNumber = Random.Range(-randomFactor, randomFactor);
            timeBetweenSpawn = Mathf.Abs(wave.GetTimeBetweenSpawns() + randomNumber); //if the time spawn in the WaveConfig have Random factor the timeBetween spawn will be different every time
            if (timeBetweenSpawn == 0) timeBetweenSpawn = 0.1f;
            yield return new WaitForSeconds(timeBetweenSpawn);
            NonFriendly enemy = Instantiate // it can be enemy or asteroid (childrens of NonFriendly)
                (wave.GetEnemyPrefab(),
                    wave.GetWayPoints(0),
                    Quaternion.identity).GetComponent(typeof(NonFriendly)) as NonFriendly;
                enemy.SetConfig(wave);
                waveManager.EnemyBorn();
            //when the last enemy of the actual wave spawn, the bool array of that wave-index gets true. If all bools of the array are true it means all waves ended
            if (enemyCount == enemiesNum - 1)
                waveSpawned[numOfWave] = true;
            
        }
    }

    private void ActualiceMaxTimeLimit(float newValue)
    {
        if (newValue > maxTimeLimit)
            maxTimeLimit = newValue;
    }

}
