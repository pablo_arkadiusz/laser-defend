﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastLevelPanel : MonoBehaviour //PANEL THAT IS CALLED AT THE END OF LEVELS (WAVE 12)
{
    [SerializeField] private PanelController panelController;

    public void DoAll() //Just call panel controller to invoke this panel and do all the work
    {
        panelController.StartCalculateReward();
        GameData.gameData.levelsUnlocked = GameData.gameData.levelSelected;
        GameData.SaveData();
    }
    



}
