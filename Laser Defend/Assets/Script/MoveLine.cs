﻿using UnityEngine;

public class MoveLine : MonoBehaviour //SCRIPT THAT MANAGE THE POINTS OF THE LINE THAT LINK THE PLANETS IN THE PLANET_MENU
{
    private LineRenderer lr;
    private byte planetsDiscovered=7;

    [SerializeField] Transform[] planetPositions;
    

    private void Start()
    {
        lr = GetComponent(typeof(LineRenderer)) as LineRenderer;
    }
    float i = 0;

    private void Update() //SET THE POINTS POSITION TO THE DISCOVERED PLANET POSITION
    {
        i -= 0.05f;
        lr.material.mainTextureOffset = new Vector2(i, 0); //CHANGE THE OFFSET OF THE LINE TEXTURE EVERY FRAME (WHICH CREATE A EFFECT OF MOVING THE POINTS  BETWEEN THE PLANETS)

        for (byte i = 0; i < planetsDiscovered; ++i) // SET A POINT TO ALL DISCOVERED PLANETS
            lr.SetPosition(i, planetPositions[i].position);
        for (byte i = planetsDiscovered; i < planetPositions.Length; ++i) //THE POINT LEFT ARE SET TO THE LAST DISCOVER PLANET
            lr.SetPosition(i, planetPositions[planetsDiscovered - 1].position);
    }
}
