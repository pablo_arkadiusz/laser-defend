﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawnManager : MonoBehaviour
{
    [Header("GameObjects")]
    [SerializeField] GameObject coin;
    [SerializeField] GameObject upgrade;
    [SerializeField] GameObject life;
    [SerializeField] GameObject[] weaponChangers;
    private Player player;

    [Header("Probabilities")]
    [SerializeField] private float probSpawnWeapon = 0.05f;
    [SerializeField] private float probSpawnUpgrade = 0.05f;
    [SerializeField] private float probSpawnLife = 0.05f;

    private void Start()
    {
        player = FindObjectOfType(typeof(Player)) as Player;
    }

    public void SpawnItems(Vector2 pos) //Called when an enemy die
    {
        TrySpawnItems(pos,probSpawnWeapon,SelectRandomWeapon());
        TrySpawnItems(pos, probSpawnUpgrade, upgrade);
        TrySpawnItems(pos, probSpawnLife, life);
        SpawnCoins(pos);
    }

    private GameObject SelectRandomWeapon()
    {
        return weaponChangers[Random.Range(0, 5)];
    }

    private void TrySpawnItems(Vector2 pos,float prob,GameObject item) //some probability to spawn an Item
    {
        float randomNumber = Random.value;
        if (randomNumber <=prob)
            InstantiateItem(item, pos);
    }

    private void SpawnCoins(Vector2 pos)
    {
        int numOfCoins;
        float randomNumber = Random.value;
        if (randomNumber <= 0.90f) numOfCoins = 1;
        else if (randomNumber <= 0.98) numOfCoins = 2;
        else numOfCoins = 3;

        for (byte i = 0; i < numOfCoins; ++i)
        {
            InstantiateItem(coin, pos);
        }
    }

    private void InstantiateItem(GameObject item,Vector2 pos)
    {
        GameObject newItem = Instantiate(item,pos,Quaternion.identity);
        Vector2 randomForce = new Vector2(Random.Range(-50, 50), 0);
        (newItem.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D).AddForce(randomForce);
    }
}
