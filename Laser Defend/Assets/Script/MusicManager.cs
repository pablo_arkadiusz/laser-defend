﻿using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioClip menuAudio;
    [SerializeField] private AudioClip gameAudio;
    [SerializeField] private AudioClip transmissionMsj;
    private AudioSource audioSouce;
    private const byte GAME_SCENE = 4;
    private const byte GAME_OVER_SCENE = 4;
    void Awake ()
    {
        SetUpSingleton();
        if(this) //if this object isnt being destroyed
            audioSouce = GetComponent(typeof(AudioSource)) as AudioSource;
	}

    private void SetUpSingleton()
    {
        //GetType() gets the type of the actual script (MusicManager in this case)
        if (FindObjectsOfType(GetType()).Length > 1)
            DestroyImmediate(gameObject); //in Game_Scene Panel_controller finds MusicMager, but if dont destroy inmediatelly it find the bad one
        else
            DontDestroyOnLoad(gameObject);
    }

    void OnLevelWasLoaded(int level)
    {
        if (level == GAME_SCENE) 
        {
            audioSouce.Stop();
        }
        else if (level != GAME_OVER_SCENE)
        {
            if (audioSouce.clip != menuAudio)
            {
                audioSouce.clip = menuAudio;
                audioSouce.Play();
            }
        }
    }

    public void StartGameAudio()
    {
        audioSouce.clip = gameAudio;
        audioSouce.Play();
    }

    public void PlayAudio(AudioClip audio,float volume)
    {
        audioSouce.PlayOneShot(audio,volume);
    }
}
