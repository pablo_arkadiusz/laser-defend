﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadManager : MonoBehaviour //TO LOAD PREVIOUS GAME OR START A NEW ONE
{
    [SerializeField] private Button btnContinue;

    private void Start()
    {
        if (!SaveLoad.CheckFileExist())
            btnContinue.interactable = false;
    }

    public void LoadGame()
    {
        GameData.LoadData();
    }

    public void NewGame()
    {
        GameData.ResetData();
    }

}
