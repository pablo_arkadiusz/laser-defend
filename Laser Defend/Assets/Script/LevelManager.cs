﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour //LEVEL MANAGER TO CHANGE SCENES AND FADE ANIMATION BETWEEN SCENES
{
    private Image fadeImg;
    enum Scene : byte { START_MENU = 1, HANGAR, PLANET_MENU, GAME, GAME_OVER }

    private void Awake()
    {
        fadeImg = GetComponentInChildren(typeof(Image)) as Image;

    }


    private void Update()
    {
        transform.position = Camera.main.transform.position; //THE FADE SPRITE IS ALWAYS ON THE CAMERA
    }

    

    public void LoadStartMenu()
    {
        Cursor.visible = true;
        StartCoroutine(FadeAndLoad((byte)Scene.START_MENU));
    }

    public void LoadHangar()
    {
        StartCoroutine(FadeAndLoad((byte)Scene.HANGAR));
    }

    public void LoadPlanetMenu()
    {
        StartCoroutine(FadeAndLoad((byte)Scene.PLANET_MENU));
    }

    public void LoadGame()
    {
        StartCoroutine(FadeAndLoad((byte)Scene.GAME));
    }

    public void LoadGameOver()
    {
        Cursor.visible = true;
        StartCoroutine(WaitAndLoad());
    }
    IEnumerator WaitAndLoad()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene((byte)Scene.GAME_OVER);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void OnLevelWasLoaded( int level)
    {
        StartCoroutine(FadeOut());
    }

    private IEnumerator FadeAndLoad(byte sceneID)
    {
        Color newColor = new Color(0, 0, 0, 0.05f);
        while (fadeImg.color.a < 1)
        {
            fadeImg.color += newColor;
            yield return new WaitForEndOfFrame();
        }
        SceneManager.LoadScene(sceneID);
    }

    private IEnumerator FadeOut()
    {
        fadeImg.color = Color.black;
        Color newColor = new Color(0, 0, 0, 0.03f);
        fadeImg = GetComponentInChildren(typeof(Image)) as Image;
        while (fadeImg.color.a >= 0)
        {
            fadeImg.color -= newColor;
            yield return new WaitForEndOfFrame();
        }
    }
}
