﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectorShip : MonoBehaviour //THE BEHAVIOUR OF THE SHIP THAT SELECT THE PLANETS IN THE PLANET_MENU_SCENE
{
    [SerializeField] private Path principalPathToFollow;
    [SerializeField] private float speed;

    [SerializeField] private Button playButton; //the ships go to the selected world
    private Transform planet;
    

    private Path planetPath;
    private int indexToGo =-1;
    private int planetNumber=-1;

    private PathFollow thisPathFollow;

    private void Awake()
    {
        thisPathFollow = GetComponent(typeof(PathFollow)) as PathFollow;
    }
   

    private IEnumerator CheckingPosition()
    {
        bool reachTarget = false;
        while(!reachTarget)
        {
            float distance = Vector2.Distance(planet.position, transform.position);
            if (distance < 2f)
            {
                reachTarget = true;
                thisPathFollow.StopFollowingPath();
                thisPathFollow.PathToFollow = planetPath;
                thisPathFollow.SetIndex(0);
                thisPathFollow.StartFollowingOnedirectionalPath();
            }
            yield return new WaitForEndOfFrame();
        }
    }


    bool forward;
    int diference;
    public void GetDirection(int index)
    {

        indexToGo = (index + 1) * 5 - 1;

        if (index == planetNumber)
        {
            diference = 0;
            return;
        }
        forward = (index > planetNumber);
        
        diference = Mathf.Abs(index - planetNumber);
        planetNumber = index;
        GameData.gameData.levelSelected = (byte)(index + 1);
        GameData.SaveData();
        playButton.interactable = true;
        (playButton.GetComponentInChildren(typeof(Text)) as Text).color = Color.white;
    }

    public void GoBackToPrincipalPath(Path path)
    {
        if (diference == 0) return;
        thisPathFollow.PathToFollow = principalPathToFollow;
        thisPathFollow.SetIndex(indexToGo);
        principalPathToFollow.InitialiceWayPoint();
        planetPath = path;
        planetPath.InitialiceWayPoint();
        thisPathFollow.speed = speed;
        planet = path.transform.parent.GetChild(0);
        thisPathFollow.StopFollowingPath();
        thisPathFollow.SetForward(forward);
        thisPathFollow.StartFollowingLoopingPath();
        StartCoroutine(CheckingPosition());
    }
    
 

}
