﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Upgrade : MonoBehaviour // ITEM THAT UPGRADES PLAYER LEVEL. WHITH EVERY LEVEL PLAYER WEAPONS GET MORE BULLETS AND BECOME STRONGER
{
    private const byte NUMBER_OF_UPGRADES = 4;
    private const byte SHREDDER_LAYER = 2;
    [SerializeField] private float rotationSpeed=5;
    [SerializeField] private AudioClip audioClip;
    private PanelController panelController;
    

    private void Update()
    {
        transform.Rotate(0, 0, rotationSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.transform.gameObject.layer == SHREDDER_LAYER) return;

        (FindObjectOfType(typeof(MusicManager)) as MusicManager).PlayAudio(audioClip,1.3f);
        panelController = FindObjectOfType(typeof(PanelController)) as PanelController;


        Player player = other.GetComponent(typeof(Player)) as Player;
        if (!player) Debug.LogError("Should not collide with not player object");
        byte newLevel= player.LevelUp();
        panelController.UpgradeLeveLTxt(newLevel);
        UpgradeNumberOfLasers(player.GetComponent(typeof(LaserController)) as LaserController,newLevel);
        Destroy(gameObject);
    }



    private void UpgradeNumberOfLasers(LaserController laser, byte newLevel)
    {
        if ((newLevel - 1) % NUMBER_OF_UPGRADES == 0 && newLevel != 1) // if the previous level is multiple of 4 then upgradeLasers (on levels 5,9,13 and 17)
            laser.CreateNewLasers();
        laser.UpdateLasersStats();
    }

}
