﻿using UnityEngine;


public enum Weapons { MachineGun, Rocket, Laser, Ripple, Ball };

public class WeaponChanger : MonoBehaviour //ITEM THAT CHANGE THE PLAYER WEAPON ON COLLISION
{
    //to choose in the inspector
    public Weapons typeOfWeapon;
    private PanelController panelController; //to actualize weapon imgage when player change weapon
    [SerializeField] private AudioClip audioClip;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.gameObject.layer == 2) return; //temporal
        Player player = other.GetComponent(typeof(Player)) as Player;
        if (!player)
        {
            Debug.LogError("Should not collide with not player object");
            Debug.LogError("other: " + other.gameObject + "layer: " + other.transform.gameObject.layer);
        }
        (FindObjectOfType(typeof(MusicManager)) as MusicManager).PlayAudio(audioClip, 4);
        player.SelectWeapon(typeOfWeapon);

        if (typeOfWeapon == Weapons.Laser)
            UpdateLaser(player.gameObject);
        else
            TurnOffLaser(player.gameObject);

        panelController = FindObjectOfType(typeof(PanelController)) as PanelController;
        Sprite weaponImg = panelController.GetWeaponImg(typeOfWeapon);
        panelController.ChangeWeaponPanelImg(weaponImg);


        Destroy(gameObject);
    }

    private void TurnOffLaser(GameObject player)
    {
        (player.GetComponent(typeof(LaserController)) as LaserController).TurnOffLasers();
    }
    private void UpdateLaser(GameObject player)
    {
        (player.GetComponent(typeof(LaserController)) as LaserController).UpdateLasersStats();
    }

 

}

