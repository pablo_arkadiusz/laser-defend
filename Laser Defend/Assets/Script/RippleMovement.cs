﻿using UnityEngine;

public class RippleMovement : MonoBehaviour
{
    [HideInInspector] public float growing_factor;

    private void Update()
    {
        if(!PauseMenu.gameIsPaused)
            transform.localScale += new Vector3(growing_factor, growing_factor, growing_factor);
    }

}
