﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    private const byte SHREDDER_LAYER = 2;


    [SerializeField] private int points;
    [SerializeField] private AudioClip audioClip; //sound of getting coin

    private ScoreManager scoreManager; //Cant be serialized because coins are instantiaded as prefabs

    

    private void Start()
    {
        scoreManager = FindObjectOfType(typeof(ScoreManager)) as ScoreManager;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Only collide with player and also with Shredders to get destroyed
        if (other.transform.gameObject.layer == SHREDDER_LAYER) return; //If it collide with shredder it will get destroyed 

        if (!other.GetComponent(typeof(Player))) Debug.LogError("Should not collide with not player object");
        AudioSource.PlayClipAtPoint(audioClip, Camera.main.transform.position, 0.5f);
        scoreManager.AddScore(points);
        Destroy(gameObject);
    }
}
