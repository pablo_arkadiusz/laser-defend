﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
    //All objects that deal damage with contact should have this (player,player bullets,enemy,enemy bullets,etc)
{
    public float Damage { set; get; } //Setted in the object main script that contains the DamageDealer. Asteroids,Enemy,Player,etc for example
    [SerializeField] private bool destroyOnCollision=true;

    public void Hit()
    {
        if(destroyOnCollision)
            Destroy(gameObject);
    }
	
}
