﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    private Transform[] wayPoint;
    [SerializeField] private bool doubleDirection = false;
    [SerializeField] private bool showInInspector = true;

    public bool IsDoubleDirection()
    {
        return doubleDirection;
    }

    void OnDrawGizmos()
    {
        if (showInInspector)
        {
            Gizmos.color = Color.red;
            Transform[] array = transform.GetComponentsInChildren<Transform>();//this array also contais the parent
            byte size = (byte)array.Length;
            wayPoint = new Transform[size - 1];
            for (byte i = 1; i < size; ++i)
                wayPoint[i - 1] = array[i]; //delete the parent from the array(the array[0])
            Gizmos.DrawWireSphere(wayPoint[0].position, 0.05f);
            for (byte i = 1; i < size - 1; ++i)
            {
                Gizmos.DrawLine(wayPoint[i - 1].position, wayPoint[i].position);
                Gizmos.DrawWireSphere(wayPoint[i].position, 0.05f);
            }
        }
    }

    //Public methods to call from PathFollow script
    public void InitialiceWayPoint()
    {
        Transform[] array = transform.GetComponentsInChildren<Transform>();//this array also contais the parent
        byte size = (byte)array.Length;
        wayPoint = new Transform[size - 1];
        for (byte i = 1; i < size; ++i)
            wayPoint[i - 1] = array[i]; //delete the parent from the array(the array[0])
    }

    public Vector2 GetWayPointPos(int i)
    {//to get position of selected wayPoint
        return wayPoint[i].position;
    }

    public int GetLength()
    {
        return wayPoint.Length;
    }


}