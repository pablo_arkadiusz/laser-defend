﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public static class SaveLoad //SAVE AND LOAD SYSTEM. IT CREATE A PERSISTENT DATA PATH IN HTE COMPUTER TO SAVE THE DATA AND TO READ FROM THAT.
                                //GAMEDATE.gamedata is the object where the data and variables are stored
{
    public static void SaveData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/SAVED.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, GameData.gameData);
        stream.Close();
    }

    public static GameData LoadData()
    {
        string path = Application.persistentDataPath + "/SAVED.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(stream) as GameData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Save file not founD in : " + path);
            return null;
        }
    }

    public static bool CheckFileExist()
    {
        string path = Application.persistentDataPath + "/SAVED.fun";
        return File.Exists(path);
    }

    public static void Delete()
    {
        try
        {
            File.Delete(Application.persistentDataPath + "/SAVED.fun");
        }
        catch (System.Exception ex)
        {
            Debug.LogException(ex);
        }
    }


}
