﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetManager : MonoBehaviour //set interactable only the planets that are unlocked
{
    private const byte NUM_OF_PANETS = 7;
    [SerializeField] private Button[] planetButton;

    private void Awake()
    {
        for(byte i=1; i<GameData.gameData.levelsUnlocked+1;++i)
        {
            if (i < NUM_OF_PANETS)
            {
                planetButton[i].interactable = true;
                planetButton[i].transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

}
