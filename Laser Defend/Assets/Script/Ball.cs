﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : WeaponClass
    //Ball is a weapon of player
{
    [Header("Proyectile")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float bulletSpeed = 10f;
    private const float BASE_DAMAGE = 10f;

    [Header("Sound")]
    [SerializeField] private AudioClip shootSFX;
    [SerializeField] [Range(0, 1)] private float shootSoundVolume = 0.7f;


    override public void Fire()
        //It choose the level of WeaponModifier and set the number of balls to spawn, the spawn offset and the rotation direction of the balls.
    {
        if (!isOnCooldown)
        {
            if (WeaponModifier == 1)
                CentralBullet();

            else if (WeaponModifier == 2)
                FireFirstRowBullet();

            else if (WeaponModifier == 3)
            {
                CentralBullet();
                FireFirstRowBullet();
            }
            else if (WeaponModifier == 4)
            {
                CentralBullet();
                FireFirstRowBullet();
                FireSecondRowBullet();
            }
            else if (WeaponModifier == 5)
            {
                CentralBullet();
                FireFirstRowBullet();
                FireSecondRowBullet();
                FireThirdRowBullet();
            }
            isOnCooldown = true;
            AudioSource.PlayClipAtPoint(shootSFX, Camera.main.transform.position, shootSoundVolume);
            StartCoroutine(chargeCooldown());
        }
    }


    private void CentralBullet()
    {
        CreateCentralBullet(bulletPrefab, bulletSpeed);
    }
    private void FireFirstRowBullet(float rotation = 0.1f) //Create Two simetrical balls. Defined in parent class(WeaponClass)
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.2f, 0, rotation, bulletSpeed);
    }
    private void FireSecondRowBullet(float rotation = 0.1f)
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.4f, 0, rotation, bulletSpeed);
    }
    private void FireThirdRowBullet(float rotation = 0.1f)
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.6f, 0, rotation, bulletSpeed);
    }

    protected override float GetBaseDamage() //Used in parent class to set the damage of intantiated ball 
    {
        return BASE_DAMAGE;
    }

    private IEnumerator chargeCooldown()
    {
        yield return new WaitForSeconds(secondAttribute);
        isOnCooldown = false;
    }
}
