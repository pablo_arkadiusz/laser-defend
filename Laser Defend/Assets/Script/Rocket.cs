﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : WeaponClass
{
    [Header("Proyectile")]
    [SerializeField] private GameObject bulletPrefab;
    private const float BASE_DAMAGE = 10f;

    [Header("Sound")]
    [SerializeField] private AudioClip shootSFX;
    [SerializeField] [Range(0, 1)] private float shootSoundVolume = 0.7f;


    override public void Fire() //CALLED FROM PLAYER. WHEN PLAYER FIRE AND HIS WEAPON IS A ROCKET, THIS METHOD SELECT THE ACTUAL MODIFIER AND SHOOT IN BASE OF THAT. 
                                //IT CHANGE THE NUMBER OF BULLETS FOR EVERY MODIFIER
    {
        if (!isOnCooldown)
        {
            CheckWeaponModifiers();
            isOnCooldown = true;
            AudioSource.PlayClipAtPoint(shootSFX, Camera.main.transform.position, shootSoundVolume);
            StartCoroutine(chargeCooldown());
        }
    }

    private void FireCentralBullet() // CREATE BULLET IN THE CENTRAL POSITION
    {
        CreateCentralBullet(bulletPrefab); //INHERITE  FROM PARENT
    }
    private void FireFirstRowBullet()// CREATE TWO SIMETRICAL BULLETS IN THE FIRST ROW
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.25f); //INHERITE  FROM PARENT
    }
    private void FireSecondRowBullet() // CREATE TWO SIMETRICAL BULLETS IN THE SECOND ROW
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.35f,-0.2f); //INHERITE  FROM PARENT
    }
    private void FireThirdRowBullet()// CREATE TWO SIMETRICAL BULLETS IN THE THIRD ROW
    {
        CreateTwoSimetricalBullets(bulletPrefab, 0.5f,-0.5f); //INHERITE  FROM PARENT
    }
    private IEnumerator chargeCooldown()
    {
        yield return new WaitForSeconds(secondAttribute);
        isOnCooldown = false;
    }

    private void CheckWeaponModifiers()
    {
        switch (WeaponModifier)
        {
            case 1:
                FireCentralBullet();
                break;

            case 2:
                FireFirstRowBullet();
                break;

            case 3:
                FireCentralBullet();
                FireFirstRowBullet();
                break;

            case 4:
                FireCentralBullet();
                FireFirstRowBullet();
                FireSecondRowBullet();
                break;

            case 5:
                FireCentralBullet();
                FireFirstRowBullet();
                FireSecondRowBullet();
                FireThirdRowBullet();
                break;
        }
    }

    protected override float GetBaseDamage()
    {
        return BASE_DAMAGE;
    }

}
