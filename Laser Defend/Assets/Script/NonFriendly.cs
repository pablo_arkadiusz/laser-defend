﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NonFriendly : MonoBehaviour //THIS ABSTRACT CLASS REPRESENT ALL OBJECT THAT ARE ENEMY OF THE PLAYER (ENEMY SHIPS AND ASTEROIDS)
{
    public abstract void SetConfig(WaveConfig waveConfig); //all nonFriendly object are spawned from WaveManager so they need set their config from WaveConfig
    protected abstract void SetPathToFollow(Path path); // al NonoFriendly objects have to follow a Path
    public abstract void Die();
    public abstract void ProcessHit(float dmg);
}
