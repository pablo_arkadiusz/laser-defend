﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GameData
    //Used to store the variables needed to load and save game progress
{
     static public GameData gameData = new GameData(); // singleton instance that stores all the data

     public uint money;
     public bool[] shipsInPosesion = new bool[13];
     public byte selectedShipID;
     public byte shipHangarPosition;
     public Weapons weaponSelected;
     public byte levelSelected;
     public byte levelsUnlocked;

    public byte shipHP=5;//the default values are for the first ship
    public float shipSpeed=0.1f;
    public float shipDMG=1;


    public static void SaveData()
    {
        SaveLoad.SaveData();
    }

    public static void LoadData()
    {
        gameData = SaveLoad.LoadData();
    }
    public static void ResetData()
    {
        gameData = new GameData();
        SaveLoad.Delete();
    }
}
