﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeBonus : MonoBehaviour 
{
    [SerializeField] private AudioClip audioClip;


    private void OnTriggerEnter2D(Collider2D other) //IF COLLIDE WITH PLAYER INCREMENTS PLAYER LIFE POINTS
    {
        if (other.transform.gameObject.layer == 2) return; // LAYER 2 IS THE LAYER OF LIMIT SHREDDERS, WHICH WILL DESTROY THIS OBJECT
        (FindObjectOfType(typeof(MusicManager)) as MusicManager).PlayAudio(audioClip,2);
        Player player = other.GetComponent(typeof(Player)) as Player;
        player.IncrementLife();
        if (!player) Debug.LogError("Should not collide with not player object");
        Destroy(gameObject);
    }


}
