﻿using UnityEngine;


//SCRIPTABLE OBECT TO CREATE ALL CONFIGURATION OF WAVES. (NUMBER OF ENEMIES, SPEED, PATH TO FOLLOW, ETC)
//THIS ITEMS ARE CREATED IN RESOURCES/WAVES AND ARE READED BY ENEMYS_PAWNER

[CreateAssetMenu(menuName = "Enemy Wave Config")]
public class WaveConfig : ScriptableObject
{

    [SerializeField] GameObject[] enemyPrefab;
    [SerializeField] bool randomSpawn;
    [SerializeField] GameObject pathPrefab;
    [SerializeField] float timeBetweenSpawns = 0.5f;
    [SerializeField] float spawnRandomFactor = 0.3f;
    [SerializeField] int numberOfEnemies = 5;
    [SerializeField] float moveSpeed = 2f;
    [SerializeField] float speedRandomFactor = 0;
    [SerializeField] bool destroyAtEndOfPath;
    [SerializeField] float maxTimeSpawning;//0 no limit

    private uint nextEnemyToSpawn;

    private uint NextEnemyToSpawn
    {
        set
        {
            nextEnemyToSpawn = value >= enemyPrefab.Length ? 0 : value;
        }
        get { return nextEnemyToSpawn; }
    }

    public int GetEnemySize()
    {
        return enemyPrefab.Length;
    }


    public GameObject GetEnemyPrefab()
    {
        GameObject enemy;
        if (randomSpawn)
        {
            int randomIndex = Random.Range(0, enemyPrefab.Length - 1);
            enemy = enemyPrefab[randomIndex];
        }
        else
        {
            enemy = enemyPrefab[nextEnemyToSpawn];
            ++NextEnemyToSpawn;
        }
        return enemy;
    }

    public Path GetPathToFollow()
    {
        return pathPrefab.GetComponent(typeof(Path)) as Path;
    }
    public Vector2 GetWayPoints(int i)
    {
        Path path = pathPrefab.GetComponent(typeof(Path)) as Path;
        path.InitialiceWayPoint();
        return path.GetWayPointPos(i);
    }

    public float GetMoveSpeed()
    {
        float randomNumber = Random.Range(-speedRandomFactor, speedRandomFactor);
        float speed = Mathf.Abs(moveSpeed + randomNumber);
        return speed;
    }

    
    public float GetTimeBetweenSpawns() { return timeBetweenSpawns; }
    public float GetSpawnRandomFactor() { return spawnRandomFactor; }
    public int GetNumberOfEnemies()     { return numberOfEnemies; }
    public bool GetDestroyAtEndOfPath() { return destroyAtEndOfPath; }
    public float GetMaxTimeSpawning()   { return maxTimeSpawning;  }




}
