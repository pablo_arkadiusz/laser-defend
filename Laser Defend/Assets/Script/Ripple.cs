﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ripple : WeaponClass //ripple is the weapon that has form of waves
{
    [Header("Proyectile")]
    [SerializeField] private GameObject bulletPrefab;
    private float bulletSpeed=5f;
    private float growing_factor;
    private const float BASE_DAMAGE = 1f;

    [Header("Sound")]
    [SerializeField] private AudioClip shootSFX;
    [SerializeField] [Range(0, 1)] private float shootSoundVolume = 0.7f;


    override public void Fire() //when player_weapon_selected is a Ripple and when player fires this method select the modifier and fire a Ripple in base of that (with different growing factor and damage)
    {
        if (!isOnCooldown)
        {
            CheckWeaponModifiers();
            FireRipple();
        }
    }

    private void FireRipple()
    {
        GameObject bullet = CreateCentralBullet(bulletPrefab, bulletSpeed);
        (bullet.GetComponent(typeof(RippleMovement)) as RippleMovement).growing_factor = this.growing_factor;
        isOnCooldown = true;
        AudioSource.PlayClipAtPoint(shootSFX, Camera.main.transform.position, shootSoundVolume);
        StartCoroutine(chargeCooldown());
    }

    private IEnumerator chargeCooldown()
    {
        yield return new WaitForSeconds(secondAttribute);
        isOnCooldown = false;
    }

    private void CheckWeaponModifiers()
    {
        switch (WeaponModifier)
        {
            case 1:
                growing_factor = 0.05f;
                break;

            case 2:
                growing_factor = 0.1f;
                break;

            case 3:
                growing_factor = 0.2f;
                break;

            case 4:
                growing_factor = 0.3f;
                break;

            case 5:
                growing_factor = 0.4f;
                break;
        }
    }

    protected override float GetBaseDamage()
    {
        return BASE_DAMAGE;
    }
}
